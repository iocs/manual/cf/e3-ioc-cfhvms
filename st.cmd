require essioc
require opcua
#require opcua 0.10.0-betaopen62541+2

## Load standard databases
iocshLoad $(essioc_DIR)/common_config.iocsh
iocshLoad("$(opcua_DIR)/opcua.iocsh", "P=$(IOCNAME):OPC,SESSION=$(SESSION="OPC1"),SESS=$(SESSION="OPC1"),SUBS=$(SUBSCRIPT="SUB1"),INET=$(OPC_SERVER="172.17.20.10"),PORT=$(OPC_PORT="21381/MatrikonOpcUaWrapper"), DEBUG=$(OPC_DEBUG=0), SUBSDEBUG=$(OPC_SUBSCRIPTION_DEBUG=0), NODESMAX=$(NODESMAX=0)")

#opcuaSession OPC1 opc.tcp://172.17.20.10:21381/MatrikonOpcUaWrapper
#opcuaOptions OPC1 sec-mode=None autoconnect=1 debug=1 nodes-max=1000 read-timeout-min=1000 read-timeout-max=120000 #write-timeout-min=1000
#opcuaSubscription SUB1 OPC1 100
#opcuaOptions SUB1 debug=1


## Load custom databases

## High voltage substations
dbLoadTemplate("HVSubstation11.substitutions", "SUBST=J1,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation11.substitutions", "SUBST=J2,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation19.substitutions", "SUBST=J3,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation19.substitutions", "SUBST=J4,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation5.substitutions", "SUBST=J5,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J6,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J7,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J8,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J9,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J10, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J11, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J12, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J13, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J14, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J15, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J16, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation7.substitutions", "SUBST=J17, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J19, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J20, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J21, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation8.substitutions", "SUBST=J24, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation12.substitutions", "SUBST=J26, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation7.substitutions", "SUBST=J27, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation4.substitutions", "SUBST=J30, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")

## Secondary substations (cryo)
dbLoadTemplate("HVSubstation7.substitutions", "SUBST=L1,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation5b.substitutions", "SUBST=L10,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")

## Secondary substations (backup)
dbLoadTemplate("HVSubstation6.substitutions", "SUBST=N1,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadTemplate("HVSubstation6.substitutions", "SUBST=N2,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")

## Low voltage substation in accelerator
dbLoadRecords("LVStation.template","SUBST=N3, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N4, SLOT=U01,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N5, SLOT=U01,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N6, SLOT=U01,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N7, SLOT=U01,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N8, SLOT=U01,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N9, SLOT=U01,  OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N10, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N11, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N12, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N13, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N14, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N15, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N16, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N17, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N18, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N19, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N20, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N21, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N22, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N23, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N24, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N25, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N26, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N27, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N28, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N29, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N30, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N31, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N32, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N33, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N34, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N35, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N36, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N37, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N38, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N39, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N40, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N41, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N42, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")

## Low voltage stations cryo
dbLoadRecords("LVStation.template","SUBST=N43, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N44, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N45, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N46, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")

## Low voltage stations experimental hall 1
dbLoadRecords("LVStation.template","SUBST=N47, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N48, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N49, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N50, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N67, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N68, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N69, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N70, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")

## Low voltage stations experimental hall 2
dbLoadRecords("LVStation.template","SUBST=N51, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N52, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N53, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N54, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")

## Low voltage stations experimental hall 3
dbLoadRecords("LVStation.template","SUBST=N55, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N56, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N57, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N58, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")

## Low voltage stations B02 campus
dbLoadRecords("LVStation.template","SUBST=N98, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N99, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")

## Low voltage stations H01 Cub
dbLoadRecords("LVStation.template","SUBST=N79, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N80, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N81, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")
dbLoadRecords("LVStation.template","SUBST=N82, SLOT=U01, OPCSUB=$(SUBSCRIPT="SUB1"), NS=$(OPCNAMESPACE="2")")

## Building aggregate
dbLoadRecords("HVBuilding.template","")

iocInit
