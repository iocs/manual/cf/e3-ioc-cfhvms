IOC for conventional facilities high voltage system (CFHVMS)

System documentation: https://confluence.esss.lu.se/display/IS/High+voltage+monitoring+system

## Prerequisites
In order to run the test suite, you must install the following:

 * python3

On CentOS 7, run the following:

```
sudo yum install -y python3
```

And the following python modules:

 * pytest
 * pyepics
 * opcua
 * run-iocsh

You can use the following pip3 commands:

```
pip3 install pytest opcua pyepics
pip3 install run-iocsh -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple
```

Finally, compile the test server for use by the test suite:

```
cd test/server
make
```

If you wantto add nodes to the test server you can do so by editing the xml file in test/server/xml, and then regenerate and compile the c-code using this command:

```
cd test/server
make nodeset
make
```

## Test Suite Components

The test setup consists of three main components:

### OPC-UA Server - open62541
A simple test opcua server, created using open62541 [1]. The server configuration currently
consists of a number of variables provided for testing purposes.

The server listens for connections on ``opc.tcp://localhost:4840`` and the simulated
signals are available in OPC UA namespace 2.

For further information on the server configuration, see [simulation server](test/server/README.md).

## Python Test Files
The pytest framework [2] is used to implement the test cases. Individual test cases are provided
as python functions (defs) in [\(test_script.py\)](test/test_script.py). Under the hood,
run_iocsh [3] and pyepics [4] are used for communication with the test IOC.

To add a new test case, simply add a new funtion (def) to [\(test_script.py\)](test/test_script.py),
ensuring that the function name begins with the prefix ``test_``

### Running the test suite
You can run the test suite from the root of the repository with the following command:

```
pytest -v test/test_script.py
```

To run a single test case:

```
pytest -v test/test_script.py::TestUnitTests::test_UT_001
```

## References
[1] https://open62541.org/

[2] https://docs.pytest.org/en/stable/

[3] https://gitlab.esss.lu.se/ics-infrastructure/run-iocsh

[4] http://pyepics.github.io/pyepics/
