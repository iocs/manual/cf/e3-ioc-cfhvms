/* WARNING: This is a generated file.
 * Any manual changes will be overwritten. */

#ifndef CFHVMSNODESET_H_
#define CFHVMSNODESET_H_


#ifdef UA_ENABLE_AMALGAMATION
# include "open62541.h"
#else
# include <open62541/server.h>
#endif



_UA_BEGIN_DECLS

extern UA_StatusCode cfhvmsNodeSet(UA_Server *server);

_UA_END_DECLS

#endif /* CFHVMSNODESET_H_ */
